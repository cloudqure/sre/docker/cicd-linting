# CI/CD Linting Image
The purpose of this repository is to build a minimal image with various liniting tools to be used in our CI/CD pipelines.

### Usage: 

```bash 
docker build -t local/cicd-linting . 
docker run --rm local/cicd-liniting yamllint --version
```

To lint a local file, simply mount the file/directory into the container and use `yamllint` to lint you files - eg. "`yamllint /tmp/samples/sample.yml`": 
```
docker run --rm -v ${PWD}:/tmp local/cicd-liniting yamllint /tmp/samples/
```