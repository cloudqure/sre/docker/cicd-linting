FROM python:3.7-alpine AS base

FROM base AS build-base
RUN apk add --no-cache curl

FROM build-base AS yamllint
ARG YAMLLINT_VERSION=v1.20.0


WORKDIR /yaml

RUN pip install yamllint==$YAMLLINT_VERSION && \
    rm -rf ~/.cache/pip

COPY ./samples samples/

CMD ["yamllint", "--version"]
